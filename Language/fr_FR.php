<?php

define('LANG', [
    //PAGE SECTION
    "HOME" => "Accueil",
    "USER" => "Utilisateur",
    "SIGNIN" => "S'enregistrer",
    "LOGIN" => "Se connecter",

    //FORM SECTION
    "SIGNIN_FORM" => "Formulaire d'enregistrement",
    "PSEUDO" => "Nom d'utilisateur",
    "EMAIL_ADDRESS" => "Adresse email",
    "PASSWORD" => "Mot de passe",
    "CONFIRM_PASSWORD" => "Confirmation du mot de passe",
    "REQUIRED_FIELD" => "Champs obligatoire",
    "REGISTER_SUBMIT" => "s'enregistrer",

    //MESSAGE SECTION
    "HOME_MESSAGE" => "Bienvenue sur ",


    //ERROR SECTION
    "TEMPLATE_KEY_MISSING" => "Erreur: une clès est manquante : ",
    "USERNAME_NULL" => "Erreur: Le nom d'utilisateur est vide",
    "LOGIN_NULL" => "Erreur: L'identifiant de connection est vide (Nom d'utilisateur ou Email)",
    "EMAIL_NULL" => "Erreur: L'email est vide",
    "PASSWORD_NULL" => "Erreur: le mot de passe est vide",
    "PASSWORD_CONFIRM_NULL" => "Erreur: la confirmation mot de passe est vide",
    "PASSWORD_DIFFERENT" => "Erreur: Le mot de passe et sa confirmation ne sont pas égaux",
    "SQL_NEW_USER_ERROR" => "Erreur: Une erreur sql c'est produit",
    "EMAIL_EXIST" => "Erreur: L'email est déjà utilisé",
    "USERNAME_EXIST" => "Erreur: Le nom d'utilisateur est déjà utilisé",

]);