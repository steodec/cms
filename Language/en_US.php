<?php

define('LANG', [
    //PAGE SECTION
    "HOME" => "Home",
    "USER" => "User",
    "SIGNIN" => "Sign-in",
    "LOGIN" => "Login",

    //FORM SECTION
    "SIGNIN_FORM" => "Registration Form",
    "PSEUDO" => "Username",
    "EMAIL_ADDRESS" => "E-mail adress",
    "PASSWORD" => "Password",
    "CONFIRM_PASSWORD" => "Password Confirmation",
    "REQUIRED_FIELD" => "Required fields",
    "REGISTER_SUBMIT" => "Register",

    //MESSAGE
    "HOME_MESSAGE" => "Welcom to ",

    //ERROR SECTION
    "TEMPLATE_KEY_MISSING" => "Error: a key is missing: ",
    "USERNAME_NULL" => "Error: Username is empty",
    "LOGIN_NULL" => "Error: The connection identifier is empty (Username or Email)",
    "EMAIL_NULL" => "Error: Email is empty",
    "PASSWORD_NULL" => "Error: Password is empty",
    "PASSWORD_CONFIRM_NULL" => "Error: password confirmation is empty",
    "PASSWORD_DIFFERENT" => "Error: Password and confirmation are not equal",
    "SQL_NEW_USER_ERROR" => "Error: A sql error occurred",
    "EMAIL_EXIST" => "Error: Email already used",
    "USERNAME_EXIST" => "Error: Username already used",
]);