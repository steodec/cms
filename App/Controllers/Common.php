<?php


/**
 * Interface Common
 */
interface Common
{
    /**
     * Common constructor.
     * @param $title
     * @param $page
     */
    function __construct(string $title, string $page);

    /**
     * @return string
     * @author pault
     */
    function GetTitle(): string;

    /**
     * @return string
     * @author pault
     */
    function GetViews();
}