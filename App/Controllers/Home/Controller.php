<?php


namespace Home;

use Common;
use Config\Config;
use Controller\Template as Template;

require_once 'config.php';
require_once Config::CONTROLLER_PATH . 'Common.php';
require_once Config::LANG_PATH;
require_once Config::CONTROLLER_PATH . 'Template.php';


/**
 * Class Controller
 * @package Home
 */
class Controller implements Common
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $page;
    /**
     * @var Template
     */
    private $template;

    /**
     * @inheritDoc
     */
    public function __construct($title, $page)
    {
        $this->title = $title;
        $this->page = "Home/" . $page;
        $this->template = new Template($this->title, $this->page, array(
            "HOME_MESSAGE" => LANG["HOME_MESSAGE"] . Config::SITE_NAME
        ));
    }

    /**
     * @inheritDoc
     */
    function GetTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    function GetViews()
    {
        echo $this->template->parseTemplate();
    }
}