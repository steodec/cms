<?php


    namespace User;


    use Common;
    use Config\Config as Config;
    use Controller\Template as Template;
    use Models\Users\Models as Users;

    require_once 'config.php';
    require_once Config::CONTROLLER_PATH . 'Common.php';
    require_once Config::CONTROLLER_PATH . 'Template.php';
    require_once Config::MODEL_PATH . 'Users/Models.php';
    require_once Config::LANG_PATH;

    /**
     * Class Signin
     * @package User
     */
    class Signin implements Common
    {

        /**
         * @var string
         */
        private $title;
        /**
         * @var string
         */
        private $page;
        /**
         * @var Template
         */
        private $template;

        private $error;

        /**
         * @inheritDoc
         */
        public function __construct (string $title, string $page)
        {
            $this->title = $title;
            $this->page = 'Users/' . $page;
        }

        /**
         * @inheritDoc
         */
        function GetTitle (): string
        {
            return $this->title;
        }

        /**
         * @inheritDoc
         */
        function GetViews ()
        {
            $this->error = (isset($_POST['register_submit'])) ? $this->createUser() : '';
            $placeholder = array(
                "SIGNINFORM" => LANG['SIGNIN_FORM'],
                "PSEUDO" => LANG['PSEUDO'],
                "EMAILADDRESS" => LANG['EMAIL_ADDRESS'],
                "PASSWORD" => LANG['PASSWORD'],
                "CONFIRMPASSWORD" => LANG['CONFIRM_PASSWORD'],
                "REQUIREDFIELD" => LANG['REQUIRED_FIELD'],
                "REGISTERSUBMIT" => LANG['REGISTER_SUBMIT']
            );
            if (!is_null($this->error)) :
                $placeholder['ERROR'] = $this->error;
            endif;
            $this->template = new Template($this->title, $this->page, $placeholder);
            echo $this->template->ParseTemplate();

        }

        private function createUser ()
        {
            $User = new Users();

            $this->error = $User->CreateUser($_POST['pseudo'], $_POST['email'], $_POST['password'], $_POST['confirm_password']);
            if (!is_null($this->error)):
                return $this->error->getMessage();
            endif;
            return NULL;
        }
    }