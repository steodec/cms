<?php


namespace User;


use Common;
use Config\Config as Config;
use Controller\Template as Template;

require_once 'config.php';
require_once Config::CONTROLLER_PATH . 'Common.php';
require_once Config::CONTROLLER_PATH . 'Template.php';
require_once Config::MODEL_PATH . 'Users/Models.php';
require_once Config::LANG_PATH;

class Controller implements Common
{

    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $page;
    /**
     * @var Template
     */
    private $template;

    /**
     * @inheritDoc
     */
    public function __construct(string $title, string $page)
    {
        $this->title = $title;
        $this->page = 'Users/' . $page;
        $this->template = new Template($this->title, $this->page);
    }

    /**
     * @inheritDoc
     */
    function GetTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    function GetViews()
    {
        if (!isset($_SESSION['logged'])):
            echo "<script>document.location.href = '/user/login'</script>";
            return;
        endif;
    }
}