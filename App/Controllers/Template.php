<?php


namespace Controller;


use Config\Config as Config;

require_once 'config.php';
require_once Config::LANG_PATH;

/**
 * Class Template
 * @package Controller
 */
class Template
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $page;
    /**
     * @var array
     */
    private $variable;

    /**
     * @var
     */
    private $error;

    /**
     * Template constructor.
     * @param $title
     * @param $page
     * @param mixed ...$variable
     */
    public function __construct($title, $page, ...$variable)
    {
        $this->title = $title;
        $this->page = Config::VIEWS_PATH . $page;
        $this->variable = $variable;
    }

    /**
     * @return string
     * @author pault
     */
    private function readPage(): string
    {
        $html = file_get_contents(Config::TEMPLATE_PATH . "template.html");
        return $html;
    }

    private function replacePlaceHolder ($file, ...$placeHolder): string
    {
        $html = file_get_contents($file);
        foreach ($placeHolder as $item):
            foreach ($item as $key => $value):
                if (is_null($value)):
                    $value = '';
                endif;
                $html = str_replace("{{" . $key . "}}", $value, $html);
            endforeach;
        endforeach;
        if (preg_match_all("/{{[A-Z]+}}/", $html, $matchs)):
            foreach ($matchs as $value):
                $html = str_replace($value, '', $html);
            endforeach;
        endif;
        return $html;
    }

    /**
     * @return string
     * @author pault
     */
    private function getNavbar (): string
    {
        $navbar = $this->replacePlaceHolder(Config::TEMPLATE_PATH . "navbar.html", array(
            "HOME" => LANG['HOME'],
            "NAVBAR_LINKS" => NULL,
            "LOGIN" => LANG['LOGIN'],
            "SIGNIN" => LANG['SIGNIN']
        ));
        return $navbar;
    }

    /**
     * @return string
     * @author pault
     */
    private function getError (): string
    {
        $err = "";
        foreach ($this->variable as $item):
            foreach ($item as $key => $value):
                if ($key == "ERROR")
                    $err = $this->replacePlaceHolder(Config::TEMPLATE_PATH . "error.html", array(
                        "ERROR" => $value,
                    ));
            endforeach;
        endforeach;
        return $err;
    }

    /**
     * @return string
     * @author pault
     */
    private function parsePage (): string
    {
        $placeholder = [];
        foreach ($this->variable as $item):
            foreach ($item as $key => $value):
                $placeholder[$key] = $value;
            endforeach;
        endforeach;
        $page = $this->replacePlaceHolder($this->page, $placeholder);
        return $page;
    }

    /**
     * @return string
     * @author pault
     */
    public function ParseTemplate (): string
    {
        $html = $this->replacePlaceHolder(Config::TEMPLATE_PATH . "template.html", array(
            "TITLE" => $this->title,
            "NAVBAR" => $this->getNavbar(),
            "COLOR" => Config::COLOR,
            "ASSETS_PATH" => Config::ASSETS_PATH,
            "SITE_NAME" => Config::SITE_NAME,
            "BODY" => $this->parsePage(),
            "ERROR" => $this->getError()
        ));
        return $html;
    }
}