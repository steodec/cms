<?php


    namespace Models\Users;

    use Config\Config as Config;
    use Error;
    use http\Encoding\Stream\Inflate;
    use Models\DataBaseModels as DB;
    use Models\Users\Interfaces as Interfaces;
    use PDO;


    require_once 'config.php';
    require_once Config::MODEL_PATH . 'DataBaseModels.php';
    require_once Config::MODEL_PATH . 'Users/Interfaces.php';
    require_once Config::LANG_PATH;

    /**
     * Class Models
     * @package Models\Users
     */
    class Models extends DB implements Interfaces
    {
        /**
         * Models constructor.
         */
        public function __construct ()
        {
            parent::__construct();
        }

        /**
         * @param string $email
         * @return bool
         * @author pault
         */
        private function checkEmail (string $email)
        {
            $checkEmail = $this->DB->prepare("SELECT email FROM users WHERE email= :email LIMIT 1");
            $checkEmail->bindParam(':email', $email, PDO::PARAM_STR);
            $checkEmail->execute();
            $emailCheck = $checkEmail->fetchAll(PDO::FETCH_ASSOC);
            return (count($emailCheck) > 0) ? TRUE : FALSE;
        }

        /**
         * @param string $username
         * @return bool
         * @author pault
         */
        private function checkUsername (string $username)
        {
            $checkUsername = $this->DB->prepare("SELECT username FROM users WHERE username= :username LIMIT 1");
            $checkUsername->bindParam(':username', $username, PDO::PARAM_STR);
            $checkUsername->execute();
            $user = $checkUsername->fetchAll(PDO::FETCH_ASSOC);
            return (count($user) > 0) ? TRUE : FALSE;
        }

        /**
         * @param string $username
         * @param string $email
         * @param string $password
         * @param string $confirmPassword
         * @return Error|null
         * @author pault
         */
        public function CreateUser (string $username, string $email, string $password, string $confirmPassword)
        {
            if ($username == '' || is_null($username)) return new Error(LANG['USERNAME_NULL']);
            if ($email == '' || is_null($email)) return new Error(LANG['EMAIL_NULL']);
            if ($password == '' || is_null($password)) return new Error(LANG['PASSWORD_NULL']);
            if ($confirmPassword == '' || is_null($confirmPassword)) return new Error(LANG['PASSWORD_CONFIRM_NULL']);
            if ($confirmPassword != $password) return new Error(LANG['PASSWORD_DIFFERENT']);
            if ($this->checkEmail($email)) return new Error(LANG['EMAIL_EXIST']);
            if ($this->checkUsername($username)) return new Error(LANG['USERNAME_EXIST']);

            $password = password_hash($password, PASSWORD_DEFAULT);

            $newUser = $this->DB->prepare("INSERT INTO users (id, username, email, password) VALUES (uuid(), :username, :email, :password)");
            $newUser->bindParam(':username', $username, PDO::PARAM_STR);
            $newUser->bindParam(':email', $email, PDO::PARAM_STR);
            $newUser->bindParam(':password', $password, PDO::PARAM_STR);
            if (!$newUser->execute()) return new Error(LANG['SQL_NEW_USER_ERROR']);
            return NULL;

        }

        public function checkAccount (string $login, string $password)
        {
            if ($login == '' || is_null($login)) return new Error(LANG['LOGIN_NULL']);
            if ($password == '' || is_null($password)) return new Error(LANG['PASSWORD_NULL']);

        }
    }