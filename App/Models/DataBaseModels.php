<?php

namespace Models;


use PDO;
use Config\Config;

require_once 'config.php';

abstract class DataBaseModels
{

    protected $DB;

    protected function __construct ()
    {
        try {
            $this->DB = new PDO("mysql:dbname=" . Config::DATABASE . ";host=" . Config::HOST . ";port=" . Config::PORT, Config::DB_USER, Config::DB_PASSWORD);
        } catch (PDOException $e) {
            return 'Connexion échouée : ' . $e->getMessage();
        }
    }
}
