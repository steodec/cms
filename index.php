<?php

namespace Config;

use Route\RouterException as RouterException;
use Route\Router as Router;
use Home\Controller as Home;
use User\Controller as User;
use User\Login as Login;
use User\Signin as Signin;

require_once 'config.php';
require_once Config::CONTROLLER_PATH . 'Router/RouteClass.php';
require_once Config::CONTROLLER_PATH . 'Router/RouterClass.php';
require_once Config::CONTROLLER_PATH . 'Router/RouterExceptionClass.php';
require_once Config::CONTROLLER_PATH . 'Home/Controller.php';
require_once Config::CONTROLLER_PATH . 'Users/Controller.php';
require_once Config::CONTROLLER_PATH . 'Users/Login.php';
require_once Config::CONTROLLER_PATH . 'Users/Signin.php';
require_once Config::LANG_PATH;

session_start();

$router = new Router($_GET['url']);


//Home Section
$router->get('/', function () {
    header('Location: /home');
    exit();
});
$router->get('/home', function () {
    $Home = new Home(LANG['HOME'], "index.html");
    $Home->GetViews();
});

//User Section
$router->get('/user', function () {
    $User = new User(LANG['USER'], 'index.html');
    $User->GetViews();
});
$router->post('/user', function () {
    $User = new User(LANG['USER'], 'index.html');
    $User->GetViews();
});
$router->get('/user/login', function () {
    $User = new Login(LANG['LOGIN'], 'login.html');
    $User->GetViews();
});
$router->post('/user/login', function () {
    $User = new Login(LANG['LOGIN'], 'login.html');
    $User->GetViews();
});
$router->get('/user/signin', function () {
    $User = new Signin(LANG['SIGNIN'], 'signin.html');
    $User->GetViews();
});
$router->post('/user/signin', function () {
    $User = new Signin(LANG['SIGNIN'], 'signin.html');
    $User->GetViews();
});

//Router RUN
try {
    $router->run();
} catch (RouterException $e) {
    var_dump($e);
}