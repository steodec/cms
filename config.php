<?php

namespace Config;

class Config
{
    /**
     * @param fr_FR | en_us
     *
     * @author pault
     */

    CONST LANG = "fr_FR";


// DataBase Section
    CONST HOST = "localhost";
    CONST PORT = "3306";
    CONST DATABASE = "armc";
    CONST DB_USER = "root";
    CONST DB_PASSWORD = "";


// Required Path
    CONST VIEWS_PATH = 'App/Views/';
    CONST CONTROLLER_PATH = 'App/Controllers/';
    CONST ERROR_PATH = 'App/Error/';
    CONST MODEL_PATH = 'App/Models/';
    CONST TEMPLATE_PATH = 'App/Template/';
    CONST ASSETS_PATH = '/App/Public/';
    CONST LANG_PATH = 'Language/' . self::LANG . ".php";
    CONST SITE_NAME = "ArMS";
    CONST COLOR = "info";

}
